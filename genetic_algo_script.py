# -*- coding: utf-8 -*-
"""
Created on Sun Feb 26 19:05:29 2017

@author: ik-be
"""
import time
import numpy as np
import matplotlib.animation as animation

from random import randint, random
from operator import add
from matplotlib import pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D #fails wsithout
import mpl_toolkits.mplot3d.axes3d as p3
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import gridspec



class GeneticAlgorithm(object):
    
    
    def __init__(self, PropertyObj):
        self.PropertyObj = PropertyObj
        self.start = time.time()
        self.points = PropertyObj.init_dots
        self.negative_dots_dict = PropertyObj.negative_dots_dict
        self.xyz_tuple = PropertyObj.xyz_tuple
        self.target = 0 #We want the total distance as small as possible
        self.world_size = PropertyObj.world_size
        self.negative_dot_weight = PropertyObj.negative_dot_weight
        
        self.retain = PropertyObj.retain
        self.random_select = PropertyObj.random_select
        self.mutate = PropertyObj.mutate
        self.generations = PropertyObj.generations
        self.population_size = PropertyObj.population_size
        self.best_individual_generation_ar = np.zeros((self.generations+1, 3))
        self.generation_nr = 0
        self.active_key_list = []
        
        
    def add_to_active_key_list(self):
        if self.generation_nr in self.negative_dots_dict.keys() and self.generation_nr not in self.active_key_list:
            self.active_key_list.append(self.generation_nr)
            
    def _eucl_length(self, origin, target, weight = 1):
        return sum([(origin[i] - target[i])**2 for i in range(3)])**0.5 * weight

    def _total_length(self, point):
        length = []
        self.add_to_active_key_list()
        for i in xrange(len(self.points)):
            length.append(self._eucl_length(point,  self.points[i]))

        for key in self.active_key_list:
            for i in xrange(len(self.negative_dots_dict[key])):
                length.append(self._eucl_length(point, self.negative_dots_dict[key][i], 
                                                        self.negative_dot_weight) )

        return sum(length)

    def _individual(self, x, y, z):
        return [randint(*x), randint(*y), randint(*z)]
    
    def _fitness(self, individual):
        total = self._total_length(individual)
        return abs(self.target - total)
        
    def _grade(self, pop):
        grade_list, individual_list = [], []
        for ind in pop:
            fit = self._fitness(ind)
            grade_list.append(fit)
            individual_list.append(ind)
            
        self.best_individual_generation_ar[self.generation_nr][:] = np.array(individual_list[np.argmin(grade_list)])
        self.generation_nr += 1
        
        summed = reduce(add, (self._fitness(x) for x in pop), 0)
        return summed / float(len(pop))
        
    def _population(self, count, xyz):
        return [self._individual(*xyz) for i in xrange(count)]    
                
    def evolve(self, pop):
        if self.generation_nr % 10 == 0 :
            print "Generation nr: {} at time: {}".format(self.generation_nr, round(time.time() - self.start, 3))
        graded = [ (self._fitness(x), x) for x in pop]
        graded = [ x[1] for x in sorted(graded)]
        retain_length = int(len(graded)*self.retain)
        parents = graded[:retain_length]
    
        # randomly add other individuals to promote genetic diversity
        for individual in graded[retain_length:]:
            if self.random_select > random():
                parents.append(individual)
    
        # mutate some individuals
        for individual in parents:
            if self.mutate > random():
                pos_to_mutate = randint(0, len(individual)-1)
                individual[pos_to_mutate] = randint(0, self.world_size)
    
        # crossover parents to create children
        parents_length = len(parents)
        desired_length = len(pop) - parents_length
        children = []
        while len(children) < desired_length:
            male = randint(0, parents_length-1)
            female = randint(0, parents_length-1)
            if male != female:
                male = parents[male]
                female = parents[female]
                half = len(male) / 2
                child = male[:half] + female[half:]
                children.append(child)
    
        parents.extend(children)
        
        return parents
    

    def optimize(self):
        population_list = []
        p = self._population(self.population_size, self.xyz_tuple)
        
        self.fitness_history = [self._grade(p),]
        for i in xrange(self.generations):
            population_list.append(p)
            p = self.evolve(p)
            self.fitness_history.append(self._grade(p))
       

class AnimatePlot(object):


    def __init__(self, GenAlgObj, SettingsObj):
        self.GenAlgObj = GenAlgObj
        self.SettingsObj = SettingsObj
        
        self.fig = plt.figure(figsize = (16, 10))
        #self.ax = p3.Axes3D(self.subplot)
        self.gs = gridspec.GridSpec(3, 1)
        self.plot_3d_ax = self.fig.add_subplot(self.gs[:2, :], projection = '3d')
        self.plot_fitness_ax = self.fig.add_subplot(self.gs[2, :])
        
        self.line_data = [self.get_line_coords(self.GenAlgObj.best_individual_generation_ar)]
        self.lines = [self.plot_3d_ax.plot(dat[0, 0:1], dat[1, 0:1], dat[2, 0:1], 
                                               linewidth = 5)[0] for dat in self.line_data]
        self.fitness_line = self.plot_fitness_ax.plot(self.GenAlgObj.fitness_history[:1])
        
        self.setup_plot()
                   
        self.ani = animation.FuncAnimation(self.fig, self.update_lines, 
                                   frames = self.GenAlgObj.generation_nr, 
                                   fargs=(self.line_data, self.lines),
                                   interval=100, blit=False)
        
        self.active_key_list = []
        
        
    def get_line_coords(self, moving_dot_coordlist):
        line_coord_list = np.zeros((3, len(moving_dot_coordlist)))
        for i in range(len(moving_dot_coordlist)):
            target = moving_dot_coordlist[i]
            for j in range(3):
                line_coord_list[j][i] = target[j]
        return line_coord_list       

    def setup_plot(self):
        # Setting the axes properties 3d plot
       
        self.plot_3d_ax.set_title('Retain: {} Random select: {} Mutate: {}'.format(self.GenAlgObj.retain,
                                                                                  self.GenAlgObj.random_select,
                                                                                  self.GenAlgObj.mutate))
        self.plot_3d_ax.set_xlim3d([0.0, self.SettingsObj.world_size])
        self.plot_3d_ax.set_xlabel('X')
        self.plot_3d_ax.set_ylim3d([0.0, self.SettingsObj.world_size])
        self.plot_3d_ax.set_ylabel('Y')
        self.plot_3d_ax.set_zlim3d([0.0, self.SettingsObj.world_size])
        self.plot_3d_ax.set_zlabel('Z')   
        self.plot_3d_ax.scatter(self.SettingsObj.init_dots[:, 0],  
                                self.SettingsObj.init_dots[:, 1], 
                                self.SettingsObj.init_dots[:, 2],
                                    c = 'b', 
                                    s = self.SettingsObj.init_dot_size)
        
        #Line plot
        self.line_plot_ylimits = [min(self.GenAlgObj.fitness_history)*0.9, 
                                      max(self.GenAlgObj.fitness_history)*1.1]
        self.plot_fitness_ax.set_ylim(*self.line_plot_ylimits)
        self.plot_fitness_ax.set_xlim(0, self.SettingsObj.generations)
        self.plot_fitness_ax.set_xlabel('Generations')
        self.plot_fitness_ax.set_ylabel('Fitness')
        for value in self.SettingsObj.negative_dot_interval:
            self.plot_fitness_ax.plot([value, value], self.line_plot_ylimits)
        
        
        
    def update_lines(self, num, dataLines, lines):
        for line, data in zip(lines, dataLines):
        # NOTE: there is no .set_data() for 3 dim data...
            line.set_color((1, 
                            1 - num / float(self.GenAlgObj.generation_nr), 
                            num / float(self.GenAlgObj.generation_nr) ))
            line.set_data(data[0:2, :num])
            line.set_3d_properties(data[2, :num])
            
        if num in self.SettingsObj.negative_dots_dict.keys():
            self.plot_3d_ax.scatter(self.SettingsObj.negative_dots_dict[num][:, 0],
                                    self.SettingsObj.negative_dots_dict[num][:, 1],
                                    self.SettingsObj.negative_dots_dict[num][:, 2],
                                    c = 'g',
                                    s = self.SettingsObj.new_dot_size)
        self.plot_3d_ax.view_init(elev=40, azim=100+num)
        
        if num > 1:
            self.fitness_line[0].set_data(range(1, num), self.GenAlgObj.fitness_history[:num-1])
        return lines   
        
    def show(self, file_name = None):
        if file_name is not None:
            self.ani.save('C:/Users/ik-be/Desktop/temp_folder/{}.mp4'.format(file_name), fps=30, extra_args=['-vcodec', 'libx264'])
        plt.show()


class WorldProperties(object):
    
    def __init__(self, boolean_arg):
        
        #Gen algo
        self.retain = 0.2
        self.random_select = 0.1
        self.mutate = 0.1
        self.generations = 200
        self.population_size = 1000
        
        #World
        self.dot_amount = 40
        self.world_size = 200
        self.xyz_tuple = [(0, self.world_size) for i in range(3)]
        self.negative_dots_activate(boolean_arg)
        
        self.negative_dots_dict = {i : np.random.random((5 , 3)) * 50 for i in self.negative_dot_interval}
        self.negative_dot_weight = 5.0

        
        #Plot
        self.init_dot_size = 20
        self.new_dot_size = 200
        
    def negative_dots_activate(self, negative_dots_bool):
        self.negative_dots_bool = negative_dots_bool
        if negative_dots_bool:
            self.negative_dot_interval = range(30, 180, 30)
            self.init_dots = np.random.random((self.dot_amount, 3)) * 50 + 150
        else:
            self.negative_dot_interval = []
            self.init_dots = np.random.random((self.dot_amount, 3)) * self.world_size
      
def make_file_name(GenAlgObj):
    retain_str = str(GenAlgObj.retain).replace('.', '_')
    random_select_str = str(GenAlgObj.random_select).replace('.', '_')
    mutate_str = str(GenAlgObj.mutate).replace('.', '_')
    dot_bool_str = str(GenAlgObj.PropertyObj.negative_dots_bool)
    return 'retain_{}_random_select_{}_mutate_str_{}_{}'.format(retain_str, random_select_str, mutate_str, dot_bool_str)


start = time.time()

PropertyObj = WorldProperties(True)
obj = GeneticAlgorithm(PropertyObj)
obj.optimize()
print "DONE: {}".format(time.time() - start)



k = AnimatePlot(obj, PropertyObj)
k.show(make_file_name(obj)) 
#k.show() 
        